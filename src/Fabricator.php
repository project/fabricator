<?php

/**
 * @file
 * Contains a Fabricator.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator;

use Drupal\fabricator\Component\Component;
use Drupal\fabricator\Factory\FactoryInterface;
use Drupal\fabricator\Exception\FabricatorException;
use Drupal\fabricator\Factory\FactoryManager;
use Drupal\fabricator\Component\BlueprintStatus;
use Drupal\fabricator\Component\ComponentManager;

/**
 * Class Fabricator
 */
class Fabricator {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Lazy loader.
   *
   * @return Fabricator
   *   This.
   */
  static public function init() {
    return new static();
  }

  /**
   * Make.
   *
   * @param string $blueprint_name
   *   Name of the blueprint to make.
   * @param string $op
   *   Name of the operation to run make on. Valid types are
   *    - FABRICATOR_OP_INSTALL
   *    - FABRICATOR_OP_CLEAN
   *    - FABRICATOR_OP_REMOVE
   *    - FABRICATOR_OP_EXISTS
   *
   * @throws FabricatorException
   */
  public function make($blueprint_name, $op = FABRICATOR_OP_INSTALL) {

    try {
      $component = ComponentManager::init()->getComponent($blueprint_name, FABRICATOR_TYPE_BLUEPRINT);
      $factory = FactoryManager::init()
        ->getFactoryForBlueprint($blueprint_name, FABRICATOR_TYPE_BLUEPRINT);

      $this->build($factory, $component, $op);

    }
    catch(FabricatorException $f) {

      watchdog_exception('Fabricator', $f, 'Could not process blueprint: %error', array(
        '%error' => $f->getMessage(),
      ));

      throw new FabricatorException();
    }
  }

  /**
   * Build a component given a Factory.
   *
   * @param FactoryInterface $factory
   *   The factory to use.
   * @param Component $component
   *   The component to build.
   * @param string $op
   *   Name of the operation to run make on. Valid types are
   *    - FABRICATOR_OP_INSTALL
   *    - FABRICATOR_OP_CLEAN
   *    - FABRICATOR_OP_REMOVE
   *    - FABRICATOR_OP_EXISTS
   */
  public function build(FactoryInterface $factory, Component $component, $op = FABRICATOR_OP_INSTALL) {

    if ($op == 'install') {
      $factory->install($component);

      BlueprintStatus::init()
        ->setBlueprintStatus($component->name, FABRICATOR_BLUEPRINT_INSTALLED);
      drupal_set_message(format_string('Installed %name', array('%name' => $component->name)));
    }
    elseif ($op == 'remove') {
      $factory->remove($component);

      BlueprintStatus::init()
        ->setBlueprintStatus($component->name, FABRICATOR_BLUEPRINT_REMOVED);
      drupal_set_message(format_string('Removed %name', array('%name' => $component->name)));
    }
    elseif ($op == 'clean') {
      $factory->clean($component);

      BlueprintStatus::init()
        ->setBlueprintStatus($component->name, FABRICATOR_BLUEPRINT_CLEAN);
      drupal_set_message(format_string('Reset %name', array('%name' => $component->name)));
    }
    elseif ($op == 'exists') {
      $factory->exists($component);
    }
  }
}
