<?php

/**
 * @file
 * Contains a
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at xtfer dot com
 * @copyright Copyright(c) 2015 Christopher Skene
 */

namespace Drupal\fabricator\Source;

use Drupal\config\Config;
use Drupal\fabricator\Component\Component;
use Drupal\fabricator\Exception\FabricatorException;

/**
 * Class Source
 * @package Drupal\fabricator\Source
 */
class Source {

  static public function init() {

    return new static();
  }

  /**
   * Parse a Source (either a blueprint or component).
   *
   * This should set the blueprint property on the Component with the
   * exported item.
   *
   * @param Component $component
   *   The component to parse a source for.
   *
   * @return mixed
   *   The source.
   *
   * @throws FabricatorException
   */
  public function parseSource(Component $component) {

    // Use the Config API to load the configuration.
    $config = Config::load();
    $export = $config->getConfigByPath($component->settings['path'], $component->settings['parser']);

    // Clean up. This used to be in the parsers, but is Fabricator only, so
    // it now lives here.
    if ($component->settings['parser'] == 'yaml') {
      $export = array('export' => $export);
    }
    elseif ($component->settings['parser'] == 'php') {
      if (isset($export['path'])) {
        unset($export['path']);
      }
    }

    if (!isset($export[$component->exportKey]) || empty($export[$component->exportKey])) {
      throw new FabricatorException('No export found');
    }

    return $export[$component->exportKey];
  }
}
