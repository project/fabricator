<?php

/**
 * @file
 * Contains an InvalidSourceException
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Exception;


/**
 * Class InvalidSourceException
 * @package Drupal\fabricator\Exception
 */
class InvalidSourceException extends FabricatorException {

}
