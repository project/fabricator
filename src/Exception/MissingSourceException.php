<?php

/**
 * @file
 * Contains a MissingSourceException
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Exception;

/**
 * Class MissingSourceException
 * @package Drupal\fabricator\Exception
 */
class MissingSourceException extends InvalidSourceException {

}
