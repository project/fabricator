<?php

/**
 * @file
 * Contains a FabricatorException
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Exception;


/**
 * Class FabricatorException
 * @package Drupal
 */
class FabricatorException extends \Exception {

}
