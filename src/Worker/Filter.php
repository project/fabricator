<?php

/**
 * @file
 * Contains a Filter worker
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Worker;

use Drupal\fabricator\Exception\FabricatorException;

class Filter {

  /**
   * Returns the machine-readable permission name for a provided text format.
   *
   * @param object|string $format
   *   An object representing a text format.
   *
   * @return bool|string The machine-readable permission name, or FALSE if the provided text
   * The machine-readable permission name, or FALSE if the provided text
   * format is malformed or is the fallback format (which is available to
   * all users).
   * @throws \Drupal\fabricator\Exception\FabricatorException
   */
  static public function permissionName($format) {

    if (is_string($format)) {
      $format = filter_format_load($format);
    }

    if (isset($format->format) && $format->format != filter_fallback_format()) {
      return 'use text format ' . $format->format;
    }

    throw new FabricatorException;
  }
}
