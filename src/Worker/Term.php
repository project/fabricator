<?php

/**
 * @file
 * Contains a
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at xtfer dot com
 * @copyright Copyright(c) 2015 Christopher Skene
 */

namespace Drupal\fabricator\Worker;

use Drupal\fabricator\Exception\FabricatorException;

/**
 * Class Term
 * @package Drupal\fabricator\Worker
 */
class Term {

  public function createTerm($vocabulary_name, array $terms) {
    $vocabs = taxonomy_vocabulary_get_names();
    if (empty($vocabs[$vocabulary_name])) {
      throw new FabricatorException('Invalid vocabulary');
    }

    // Save taxonomy terms.
    $vid = $vocabs[$vocabulary_name]->vid;

    foreach ($terms as $name) {
      $term = new \stdClass();
      $term->name = $name;
      $term->vid = $vid;
      $term->vocabulary_machine_name = 'tags';
      taxonomy_term_save($term);
    }
  }
}
