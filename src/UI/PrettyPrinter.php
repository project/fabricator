<?php

/**
 * @file
 * Contains a Pretty Printer
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\UI;


/**
 * Class PrettyPrinter
 * @package Drupal\fabricator\UI
 */
class PrettyPrinter {

  /**
   * Main function call.
   *
   * @param array $input
   *   The array to pretty print.
   *
   * @return string
   *   A readable representation.
   */
  static public function prettyPrint($input) {

    $printer = new static();

    return $printer->render($input);
  }

  /**
   * Wraps the render in dl tags.
   *
   * @param array $input
   *   The array to pretty print.
   *
   * @return string
   *   A readable representation.
   */
  public function render($input) {
    $output = $this->renderArray($input);

    return $output;
  }

  /**
   * Renders the array.
   *
   * @param array $input
   *   The array to pretty print.
   * @param string $output
   *   The output for concatenation. Does not need to be supplied.
   *
   * @return string
   *   A readable representation.
   */
  public function renderArray($input, $output = '') {
    if (!empty($input)) {
      $output = '<dl>';

      foreach ($input as $key => $value) {
        $output .= '<dt>' . check_plain($key) . '</dt><dd>';
        if (is_array($value) || is_object($value)) {
          $output .= $this->renderArray($value);
        }
        else {
          $output .= check_plain($value);
        }

        $output .= '</dd>';
      }
      $output .= '</dl>';
    }

    return $output;
  }
}
