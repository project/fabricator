<?php

/**
 * @file
 * Contains a ComponentManager
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Component;

use Drupal\fabricator\Exception\FabricatorException;
use Drupal\fabricator\Exception\InvalidSourceException;

/**
 * Class ComponentManager
 * @package Drupal\fabricator\Component
 */
class ComponentManager {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Lazy constructor.
   *
   * @return ComponentManager
   *   This object.
   */
  static public function init() {
    return new static();
  }

  /**
   * Get the name of the source hook.
   *
   * @param string $type
   *   The type to return the hook for.
   *
   * @return string
   *   The hook name.
   */
  protected function sourceHook($type = FABRICATOR_TYPE_COMPONENT) {

    switch ($type) {
      case FABRICATOR_TYPE_COMPONENT:
        return 'fabricator_components';

      case FABRICATOR_TYPE_BLUEPRINT:
        return 'fabricator_blueprints';

      case FABRICATOR_TYPE_SET:
        return 'fabricator_sets';
    }
  }

  /**
   * Fetch a component.
   *
   * @param string $name
   *   The name of the blueprint.
   * @param string $type
   *   The type. Valid types are:
   *    - FABRICATOR_TYPE_BLUEPRINT
   *    - FABRICATOR_TYPE_COMPONENT
   *
   * @throws FabricatorException
   * @throws InvalidSourceException
   * @return Component
   *   The component
   */
  public function getComponent($name, $type = FABRICATOR_TYPE_COMPONENT) {

    $settings = $this->getComponentSettings($name, $type);

    $component = Component::create($name, $settings, $type);

    return $component;
  }

  /**
   * Determine if a blueprint name is valid.
   *
   * @param string $name
   *   The name to check
   * @param string $type
   *   The type. Valid types are:
   *    - FABRICATOR_TYPE_BLUEPRINT
   *    - FABRICATOR_TYPE_COMPONENT
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isValid($name, $type = FABRICATOR_TYPE_COMPONENT) {

    $sources = $this->getComponents($type);
    if (array_key_exists($name, $sources)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get blueprints.
   *
   * @return array
   *   An array of blueprints.
   */
  public function getBlueprints() {

    return $this->getComponents(FABRICATOR_TYPE_BLUEPRINT);
  }

  /**
   * Fetch information about available things.
   *
   * @param string $type
   *   The type. Valid types are:
   *    - FABRICATOR_TYPE_BLUEPRINT
   *    - FABRICATOR_TYPE_COMPONENT
   * @param bool $reset
   *   TRUE to reload.
   *
   * @return array
   *   An array of components
   */
  public function getComponents($type = FABRICATOR_TYPE_COMPONENT, $reset = FALSE) {

    $sorted_components = &drupal_static('fabricator_' . $this->sourceHook($type));
    $implementers = &drupal_static('fabricator_' . $this->sourceHook($type) . '_implementers');

    if (!empty($sorted_components) && $reset == FALSE) {
      return $sorted_components;
    }

    $implementers = module_implements($this->sourceHook($type));
    if (empty($implementers)) {
      return array();
    }

    $sorted_components = array();
    foreach ($implementers as $module_name) {

      $components = module_invoke($module_name, $this->sourceHook($type));
      if (empty($components)) {
        continue;
      }

      foreach ($components as $key => $component) {
        $machine_name = $module_name . '_' . strtolower($key);
        $defaults = array(
          'name' => '',
          'type' => isset($component['type']) ? $component['type'] : 'none',
          'provider' => $module_name,
          'machine_name' => $machine_name,
          'description' => '',
        );

        $component['path'] = drupal_get_path('module', $module_name) . '/' . $component['file'];

        $sorted_components[$machine_name] = array_merge($defaults, $component);
      }
    }

    return $sorted_components;
  }

  /**
   * Get an individual component.
   *
   * @param string $name
   *   Name of the blueprint
   * @param string $type
   *   The type. Valid types are:
   *    - FABRICATOR_TYPE_BLUEPRINT
   *    - FABRICATOR_TYPE_COMPONENT
   *
   * @throws InvalidSourceException
   * @return array
   *   The individual blueprint settings.
   */
  public function getComponentSettings($name, $type = FABRICATOR_TYPE_COMPONENT) {

    $sources = $this->getComponents($type);
    if (array_key_exists($name, $sources)) {
      return $sources[$name];
    }

    throw new InvalidSourceException(format_string('Invalid source !name', array(
      '!name' => $name,
    )));
  }

  /**
   * Add components to a component.
   *
   * @param \Drupal\fabricator\Component\Component $component
   *   Component to add components to.
   */
  protected function addComponents(Component $component) {

    if (empty($component->components)) {
      return;
    }

    // Support normal drupal weight sorting.
    uasort($component->components, 'drupal_sort_weight');

    // Include components according to weight.
    foreach ($component->components as $nested_component) {

      if (is_array($nested_component)) {
        $component_name = $nested_component['name'];
      }
      else {
        $component_name = $nested_component;
      }

      try {
        $new_component = $this->getComponent($component_name, FABRICATOR_TYPE_COMPONENT);
        $component->blueprint = array_replace_recursive($component->blueprint, $new_component->blueprint);
      }
      catch (InvalidSourceException $e) {
        watchdog_exception('fabricator', $e, 'Broken/missing component !name was skipped', array(
          '!name' => $component_name,
        ));
      }
    }
  }
}
