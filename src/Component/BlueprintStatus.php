<?php

/**
 * @file
 * Contains a BlueprintStatus handler
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Component;

use Drupal\fabricator\Component\ComponentManager;
use Drupal\fabricator\Factory\FactoryManager;

/**
 * Class BlueprintStatus
 * @package Drupal\fabricator\Blueprint
 */
class BlueprintStatus {

  /**
   * A blueprint manager.
   *
   * @var ComponentManager
   */
  public $manager;

  /**
   * Constructor.
   *
   * @param ComponentManager $manager
   *   A ComponentManager
   */
  public function __construct(ComponentManager $manager) {
    $this->manager = $manager;
  }

  /**
   * Lazy constructor.
   *
   * @return BlueprintStatus
   *   This object.
   */
  static public function init() {
    $manager = new ComponentManager();
    return new static($manager);
  }

  /**
   * Get the status of a blueprint.
   *
   * @param string $name
   *   Name of the blueprint.
   *
   * @return int
   *   The status. Can be one of:
   *    - FABRICATOR_BLUEPRINT_CLEAN
   *    - FABRICATOR_BLUEPRINT_INSTALLED
   *    - FABRICATOR_BLUEPRINT_REMOVED
   *    - FABRICATOR_BLUEPRINT_CONFLICT
   */
  public function getBlueprintStatus($name) {

    $blueprint = ComponentManager::init()->getComponent($name, FABRICATOR_TYPE_BLUEPRINT);
    $factory = FactoryManager::init()->getFactoryForBlueprint($name, FABRICATOR_TYPE_BLUEPRINT);

    $status = $factory->exists($blueprint);

    if (empty($status)) {
      return FABRICATOR_BLUEPRINT_CLEAN;
    }

    return FABRICATOR_BLUEPRINT_INSTALLED;
  }

  /**
   * Get the readable status of a blueprint.
   *
   * @param string $name
   *   Name of the blueprint.
   *
   * @return string
   *   Human readable string.
   */
  public function getReadableStatus($name) {
    $status = $this->getBlueprintStatus($name);

    switch ($status) {

      case FABRICATOR_BLUEPRINT_INSTALLED:
        return t('Built');

      case FABRICATOR_BLUEPRINT_REMOVED:
        return t('Removed');

      case FABRICATOR_BLUEPRINT_CONFLICT:
        return t('Conflict');

      case FABRICATOR_BLUEPRINT_CLEAN:
      default:
        return t('Installable');
    }
  }

  /**
   * Set the blueprint status.
   *
   * @param string $blueprint_name
   *   Name of the blueprint.
   * @param int $state
   *   Its state. One of FABRICATOR_BLUEPRINT_INSTALLED,
   *    FABRICATOR_BLUEPRINT_REMOVED,
   *    FABRICATOR_BLUEPRINT_CLEAN.
   */
  public function setBlueprintStatus($blueprint_name, $state) {

    $status = variable_get('fabricator_blueprint_status', array());

    $status[$blueprint_name] = $state;

    variable_set('fabricator_blueprint_status', $status);
  }
}
