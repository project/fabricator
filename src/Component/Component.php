<?php

/**
 * @file
 * Contains a Component
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Component;

use Drupal\fabricator\Exception\InvalidSourceException;
use Drupal\config\Parser\ParserManager;
use Drupal\fabricator\Source\Source;

/**
 * Class Component
 * @package Drupal\fabricator
 */
class Component {

  /**
   * The type.
   *
   * @var string
   */
  public $type;

  /**
   * Name.
   *
   * @var string
   */
  public $name;

  /**
   * Settings.
   *
   * @var array
   */
  public $settings;

  /**
   *
   *
   * @var string
   */
  public $exportKey;

  /**
   * The blueprint
   *
   * @var mixed
   */
  public $blueprint;

  /**
   * Any included components
   *
   * @var array
   */
  public $components = array();

  /**
   * Constructor.
   *
   * @param string $name
   *   Name of the component
   * @param array $settings
   *   The settings
   * @param string $type
   *   The type of component.
   */
  public function __construct($name, array $settings = array(), $type = FABRICATOR_TYPE_COMPONENT) {

    $this->name = $name;
    $this->settings = $settings;
    $this->type = $type;

    $this->exportKey = 'export';
    if (isset($settings['ctools'])) {
      $this->exportKey = $settings['ctools']['export_key'];
    }
  }

  /**
   * Create and build a component.
   *
   * @param string $name
   *   Name of the component
   * @param array $settings
   *   The settings
   * @param string $type
   *   The type of component.
   *
   * @return Component
   *   A fully populated and build component.
   */
  static public function create($name, array $settings = array(), $type = FABRICATOR_TYPE_COMPONENT) {

    $component = new static($name, $settings, $type);
    $component->rebuild();

    return $component;
  }

  /**
   * Rebuild the blueprint data for this component.
   *
   * @throws \Drupal\fabricator\Exception\FabricatorException
   */
  public function rebuild() {

    $source = Source::init()->parseSource($this);

    $this->setBlueprint($source);

    $this->rebuildComponents();
  }

  /**
   * Set the blueprint.
   *
   * @param mixed $blueprint
   *   The blueprint.
   */
  public function setBlueprint($blueprint) {

    if (is_array($blueprint) && isset($blueprint['components']) && !empty($blueprint['components'])) {
      $this->components = $blueprint['components'];
      unset($blueprint['components']);
    }

    $this->blueprint = $blueprint;
  }

  /**
   * Add components to this component.
   *
   * @param array $components
   *   An array of Components.
   */
  public function addComponents($components) {

    if (!empty($components)) {
      foreach ($components as $key => $component) {
        $this->addComponent($key, $component);
      }
    }
  }

  /**
   * Add an individual component.
   *
   * @param string $name
   *   Name of the component
   * @param Component $component
   *   The component
   */
  public function addComponent($name, Component $component) {

    $this->components[$name] = $component;
  }

  /**
   * Remove a component.
   *
   * @param string $name
   *   The name.
   */
  public function removeComponent($name) {
    if (isset($this->components[$name])) {
      unset($this->components[$name]);
    }
  }

  /**
   * Rebuilds included components based on the existing component list.
   */
  public function rebuildComponents() {

    if (empty($this->components)) {
      return;
    }

    $component_manager = ComponentManager::init();

    // Support normal drupal weight sorting.
    uasort($this->components, 'drupal_sort_weight');

    // Include components according to weight.
    foreach ($this->components as $nested_component_index => $nested_component) {

      if (is_array($nested_component)) {
        $component_name = $nested_component_index;
      }
      else {
        $component_name = $nested_component;
      }

      try {
        $new_component = $component_manager->getComponent($component_name, FABRICATOR_TYPE_COMPONENT);
        $this->blueprint = array_replace_recursive($this->blueprint, $new_component->blueprint);
      }
      catch (InvalidSourceException $e) {
        watchdog_exception('fabricator', $e, 'Broken/missing component !name was skipped', array(
          '!name' => $component_name,
        ));
      }
    }

  }
}
