<?php

/**
 * @file
 * Contains a BlueprintOps Component.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Component;

/**
 * Class BlueprintOps
 * @package Drupal\fabricator\Blueprint
 */
class BlueprintOps {

  /**
   * Constructor.
   */
  public function __construct() {
  }

  /**
   * Lazy constructor.
   *
   * @return BlueprintOps
   *   This object.
   */
  static public function init() {
    return new static();
  }

  /**
   * Get operations for a blueprint.
   *
   * @param string $name
   *   Name of the blueprint.
   *
   * @return array
   *   An array of links to operations for that blueprint.
   */
  public function getBlueprintOperations($name) {

    $ops = array();
    $status = BlueprintStatus::init()->getBlueprintStatus($name);

    if ($status == FABRICATOR_BLUEPRINT_CLEAN) {
      $ops = array('install');
    }
    elseif ($status == FABRICATOR_BLUEPRINT_INSTALLED) {
      // $ops = array('remove');
    }
    elseif ($status == FABRICATOR_BLUEPRINT_REMOVED) {
      $ops = array('install', 'clean');
    }
    elseif ($status == FABRICATOR_BLUEPRINT_CONFLICT) {
      $ops = array();
    }

    $ops[] = 'view';

    return $this->buildOperations($ops, $name);
  }

  /**
   * Build operations for a blueprint.
   *
   * @param array $tasks
   *   An array of tasks
   * @param string $name
   *   Name of the blueprint.
   *
   * @return array
   *   An array of links to operations.
   */
  public function buildOperations($tasks, $name) {
    $ops = array();
    if (!empty($tasks)) {

      foreach ($tasks as $task) {
        $ops[] = l(ucfirst($task), 'admin/structure/fabricator/blueprint/' . $name . '/' . $task);
      }
    }

    return $ops;
  }
}
