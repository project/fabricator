<?php

/**
 * @file
 * Contains a CtoolsExportable Factory
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Factory;

use Drupal\fabricator\Component\BlueprintStatus;
use Drupal\fabricator\Component\Component;

/**
 * Class CtoolsExportable
 * @package Drupal\fabricator\Factory
 */
class CtoolsExportable extends FactoryBase implements FactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function install(Component $component) {

    ctools_include('export');

    ctools_export_crud_save($component->settings['ctools']['export'], $component->blueprint);
  }

  /**
   * {@inheritdoc}
   */
  public function exists(Component $component) {

    $result = ctools_export_crud_load($component->settings['ctools']['export'], $component->settings['ctools']['name']);

    if (!empty($result)) {
      BlueprintStatus::init()->setBlueprintStatus($component->name, FABRICATOR_BLUEPRINT_INSTALLED);

      return TRUE;
    }

    BlueprintStatus::init()->setBlueprintStatus($component->name, FABRICATOR_BLUEPRINT_CLEAN);

    return FALSE;
  }

  /**
   * Helper function to return various ctools information for components.
   *
   * Stolen from Features.
   *
   * @see _ctools_features_get_info()
   *
   * @param bool $reset
   *   (Optional) Whether to reset.
   *
   * @return array
   *   Information about available ctools exports.
   */
  public function getCtoolsComponents($reset = FALSE) {

    static $components;

    if (!isset($components) || $reset) {
      $components = array();
      $modules = features_get_info();
      ctools_include('export');
      drupal_static('ctools_export_get_schemas', NULL, $reset);

      foreach (ctools_export_get_schemas_by_module() as $module => $schemas) {
        foreach ($schemas as $table => $schema) {
          if ($schema['export']['bulk export']) {
            // Let the API owner take precedence as the owning module.
            $api_module = isset($schema['export']['api']['owner']) ? $schema['export']['api']['owner'] : $module;
            $components[$table] = array(
              'name' => isset($modules[$api_module]->info['name']) ? $modules[$api_module]->info['name'] : $api_module,
              'default_hook' => $schema['export']['default hook'],
              'default_file' => FEATURES_DEFAULTS_CUSTOM,
              'module' => $api_module,
            );
            if (isset($schema['export']['api'])) {
              $components[$table] += array(
                'api' => $schema['export']['api']['api'],
                'default_filename' => $schema['export']['api']['api'],
                'current_version' => $schema['export']['api']['current_version'],
              );
            }
          }
        }
      }
    }

    return $components;
  }

  /**
   * Get information about a specific ctools export.
   *
   * @param string|null $identifier
   *   (Optional) An identifier.
   *
   * @return array|bool
   *   The information, or FALSE if none was found.
   */
  public function getCtoolsComponent($identifier) {
    $components = $this->getCtoolsComponents();

    // Identified by the table name.
    if (isset($components[$identifier])) {
      return $components[$identifier];
    }

    // New API identifier. Allows non-exportables related CTools APIs to be
    // supported by an explicit `module:api:current_version` key.
    elseif (substr_count($identifier, ':') === 2) {
      list($module, $api, $current_version) = explode(':', $identifier);

      // If a schema component matches the provided identifier, provide that
      // information. This also ensures that the version number is up to date.
      foreach ($components as $table => $info) {
        if ($info['module'] == $module && $info['api'] == $api && $info['current_version'] >= $current_version) {
          return $info;
        }
      }

      // Fallback to just giving back what was provided to us.
      return array(
        'module' => $module,
        'api' => $api,
        'current_version' => $current_version,
      );
    }

    return FALSE;
  }
}
