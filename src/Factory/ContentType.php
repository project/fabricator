<?php

/**
 * @file
 * Contains a ContentType Factory.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Factory;

use Drupal\fabricator\Component\BlueprintStatus;
use Drupal\fabricator\Component\Component;

/**
 * Class ContentType
 * @package Drupal\fabricator\Factory
 */
class ContentType extends FactoryBase implements FactoryInterface {

  /**
   * Build the item.
   *
   * @param Component $component
   *   A component.
   *
   * @throws \Exception
   * @throws \FieldException
   */
  public function install(Component $component) {

    $blueprint = $component->blueprint;

    // Insert default pre-defined node types into the database. For a complete
    // list of available node type attributes, refer to the node type API
    // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
    if (node_type_get_type($blueprint['type']) != FALSE) {
      BlueprintStatus::init()->setBlueprintStatus($component->name, FABRICATOR_BLUEPRINT_CONFLICT);

      return;
    }

    $blueprint['module'] = 'fabricator';

    $type = node_type_set_defaults($blueprint);
    node_type_save($type);

    // Add a body field?
    if (isset($blueprint['body_field']) && !empty($blueprint['body_field'])) {
      node_add_body_field($type);
    }

    // Insert default pre-defined RDF mapping into the database.
    if (!isset($blueprint['rdf_mappings']) && !empty($blueprint['rdf_mappings'])) {
      $rdf_mappings = $blueprint['rdf_mappings'];
      foreach ($rdf_mappings as $rdf_mapping) {
        rdf_mapping_save($rdf_mapping);
      }
    }

    if (isset($blueprint['variables']) && !empty($blueprint['variables'])) {
      foreach ($blueprint['variables'] as $key => $value) {
        variable_set($key . '_' . $blueprint['type'], $value);
      }
    }

    if (isset($blueprint['fields']) && !empty($blueprint['fields'])) {
      foreach ($blueprint['fields'] as $key => $field) {

        if (field_info_field($key) == NULL) {
          field_create_field($field);
        }

        $default_instance = array();
        if (isset($blueprint['instances'][$key])) {
          $default_instance = $blueprint['instances'][$key];
        }

        $instance = array(
          'field_name' => $field['field_name'],
          'entity_type' => 'node',
          'label' => isset($field['label']) ? $field['label'] : $field['field_name'],
          'bundle' => $blueprint['type'],
          'required' => isset($field['required']) ? $field['required'] : FALSE,
          'settings' => isset($field['settings']) ? $field['settings'] : array(),
          'widget' => isset($field['widget']) ? $field['widget'] : array(),
        );

        $instance = array_replace_recursive($instance, $default_instance);

        field_create_instance($instance);
      }
    }
  }

  /**
   * Check if a thing exists.
   *
   * @param Component $component
   *   The component.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function exists(Component $component) {

    if (node_type_get_type($component->blueprint['type']) != FALSE) {
      BlueprintStatus::init()->setBlueprintStatus($component->name, FABRICATOR_BLUEPRINT_INSTALLED);

      return TRUE;
    }

    BlueprintStatus::init()->setBlueprintStatus($component->name, FABRICATOR_BLUEPRINT_CLEAN);

    return FALSE;
  }
}
