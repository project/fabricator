<?php

/**
 * @file
 * Contains an Entity Factory
 */

namespace Drupal\fabricator\Factory;

use Drupal\fabricator\Component\Component;
use Drupal\fabricator\Exception\InvalidSourceException;

/**
 * Class Entity
 * @package Drupal\fabricator\Factory
 */
class Entity extends FactoryBase implements FactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function install(Component $component) {

    $entity_values = $component->blueprint;

    if (empty($entity_values['entity_type'])) {
      throw new InvalidSourceException('You must provide a value for entity_type');
    }
    $entity_type = $entity_values['entity_type'];
    $entity = entity_create($entity_type, $entity_values);

    // Deal with image/file fields.
    list(,, $bundle) = entity_extract_ids($entity_type, $entity);
    $fields = array_filter(field_info_field_map(), function($item) use ($bundle, $entity_type) {

      return in_array($item['type'], array('file', 'image'), TRUE) && !empty($item['bundles'][$entity_type]) && in_array($bundle, $item['bundles'][$entity_type], TRUE);
    });

    // @todo: This wont work. Specifically __DIR__.
    foreach (array_keys($fields) as $field_name) {
      if (!empty($entity->{$field_name})) {
        if (($handle = fopen(__DIR__ . '/files/' . $entity->{$field_name}, 'r')) &&
          $file = file_save_data($handle, 'public://' . $entity->{$field_name})) {
          fclose($handle);
          $entity->{$field_name} = array(LANGUAGE_NONE => array(array('fid' => $file->fid)));
        }
      }
    }

    // Special case paragraph items.
    if (!empty($entity_values['host_node']) &&
      $host_candidates = node_load_multiple(array(), array('title' => $entity_values['host_node']))) {
      $host = reset($host_candidates);
      $entity->setHostEntity('node', $host);
    }

    entity_save($entity_type, $entity);
  }

}
