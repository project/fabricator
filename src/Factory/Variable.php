<?php

/**
 * @file
 * Contains a Variable factory
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Factory;

use Drupal\fabricator\Component\Component;
use \Drupal\fabricator\Worker\Variable as VariableWorker;

/**
 * Class Variable
 * @package Drupal\fabricator\Factory
 */
class Variable extends FactoryBase {

  /**
   * {@inheritdoc}
   */
  public function install(Component $component) {

    $worker = new VariableWorker();

    if (isset($component->blueprint) && !empty($component->blueprint)) {
      foreach ($component->blueprint as $key => $value) {
        $worker->set($key, $value);
      }
    }

  }

}
