<?php

/**
 * @file
 * Contains a FactoryInterface
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Factory;

use Drupal\fabricator\Component\Component;

/**
 * Interface FactoryInterface
 * @package Drupal\fabricator\Factory
 */
interface FactoryInterface {

  /**
   * Build the item.
   *
   * @param Component $component
   *   A component
   */
  public function install(Component $component);

  /**
   * Remove.
   *
   * @param Component $component
   *   A component
   */
  public function remove(Component $component);

  /**
   * Clean.
   *
   * @param Component $component
   *   A component
   */
  public function clean(Component $component);

  /**
   * Check if a thing exists.
   *
   * @param Component $component
   *   A component
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function exists(Component $component);
}
