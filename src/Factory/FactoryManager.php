<?php

/**
 * @file
 * Contains a FactoryManager
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Factory;

use Drupal\fabricator\Exception\FabricatorException;
use Drupal\fabricator\Component\ComponentManager;

/**
 * Class FactoryManager
 * @package Drupal\fabricator\Factory
 */
class FactoryManager {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Lazy constructor.
   *
   * @return FactoryManager
   *   This object.
   */
  static public function init() {
    return new static();
  }

  /**
   * Fetch information about available factories.
   *
   * @return array
   *   An array of factories.
   */
  public function getFactories() {

    $implementers = module_implements('fabricator_factories');
    if (empty($implementers)) {
      return array();
    }

    $factories = array();
    foreach ($implementers as $module_name) {

      $module_factories = module_invoke($module_name, 'fabricator_factories');
      if (empty($module_factories)) {
        continue;
      }

      foreach ($module_factories as $key => $module_factory) {
        $defaults = array(
          'name' => '',
          'type' => NULL,
          'provider' => $module_name,
          'machine_name' => $key,
        );

        $factories[$key] = array_merge($defaults, $module_factory);
      }
    }

    return $factories;
  }

  /**
   * Gets a factory for a blueprint.
   *
   * @param string $blueprint_name
   *   Name of the blueprint
   *
   * @return FactoryInterface
   *   A Factory.
   *
   * @throws FabricatorException
   */
  public function getFactoryForBlueprint($blueprint_name) {

    $blueprint = ComponentManager::init()->getComponentSettings($blueprint_name, FABRICATOR_TYPE_BLUEPRINT);
    $type = $blueprint['type'];

    if (empty($type)) {
      $type = 'none';
    }

    return $this->getFactory($type);
  }

  /**
   * Gets a factory.
   *
   * @param string $type
   *   Name of the factory
   *
   * @return FactoryInterface
   *   A Factory.
   *
   * @throws FabricatorException
   */
  public function getFactory($type) {

    $factories = $this->getFactories();
    if (array_key_exists($type, $factories)) {
      $factory_settings = $factories[$type];

      if (class_exists($factory_settings['class'])) {
        $factory = new $factory_settings['class']();
        if ($factory instanceof FactoryInterface) {
          return $factory;
        }
      }
    }

    throw new FabricatorException(format_string('Invalid Factory !factory', array(
      '!factory' => $type,
    )));
  }
}
