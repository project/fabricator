<?php

/**
 * @file
 * Contains a NullFactory
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Factory;


/**
 * Class NullFactory
 * @package Drupal\fabricator\Factory
 */
class NullFactory extends FactoryBase implements FactoryInterface {

}
