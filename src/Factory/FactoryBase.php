<?php

/**
 * @file
 * Contains a FactoryBase
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

namespace Drupal\fabricator\Factory;

use Drupal\fabricator\Component\Component;

/**
 * Class FactoryBase
 * @package Drupal\fabricator\Factory
 */
class FactoryBase implements FactoryInterface {

  /**
   * Build the item.
   *
   * @param Component $component
   *   A component
   */
  public function install(Component $component) {
    // Nothing.
  }

  /**
   * Remove.
   *
   * @param Component $component
   *   A component
   */
  public function remove(Component $component) {
    // Nothing.
  }

  /**
   * Clean.
   *
   * @param Component $component
   *   A component
   */
  public function clean(Component $component) {
    // Nothing.
  }

  /**
   * Check if a thing exists.
   *
   * @param Component $component
   *   A component
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function exists(Component $component) {
    // Nothing.
  }

}
