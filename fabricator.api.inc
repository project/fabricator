<?php

/**
 * @file
 * Fabricator API
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

/**
 * Defines components to be used on blueprints.
 *
 * Components are reusable parts of blueprints, but cannot be built on their
 * own.
 *
 * @return array
 *   An array whose keys are component names and whose corresponding values
 *   are arrays containing the following key-value pairs:
 *   - name: The human-readable name of the component, to be shown on the
 *     permission administration page. This should be wrapped in the t()
 *     function so it can be translated.
 *   - description: The description of the component shown on the component
 *     overview page. Should be wrapped in t().
 *   - parser: The parser which will ingest and convert this source into a
 *     php array. Default parsers are FABRICATOR_PARSER_PHP and
 *     FABRICATOR_PARSER_YAML, however other parsers can be declared with
 *     hook_fabricator_parsers().
 *   - file: The two provided php and yaml parsers both expect to load a file.
 *     This file should be relative to the module directory providing the
 *     component.
 */
function hook_fabricator_components() {

  $components = array();

  $components['my_image_field'] = array(
    'name' => t('Image Field'),
    'parser' => CONFIG_PARSER_YAML,
    'file' => 'blueprints/my_image_field.yml',
    'description' => t('Provides an image field'),
  );

  return $components;
}

/**
 * Defines blueprints which are available to fabricator.
 *
 * Blueprints are buildable components which can be installed on the site, and
 * can contain other components.
 *
 * @return array
 *   An array whose keys are blueprint names and whose corresponding values
 *   are arrays containing the following key-value pairs:
 *   - name: The human-readable name of the component, to be shown on the
 *     permission administration page. This should be wrapped in the t()
 *     function so it can be translated.
 *   - description: The description of the component shown on the component
 *     overview page. Should be wrapped in t().
 *   - parser: The parser which will ingest and convert this source into a
 *     php array. Default parsers are FABRICATOR_PARSER_PHP and
 *     FABRICATOR_PARSER_YAML, however other parsers can be declared with
 *     hook_fabricator_parsers().
 *   - file: The two provided php and yaml parsers both expect to load a file.
 *     This file should be relative to the module directory providing the
 *     component.
 *   - ctools: Chaos tools exports expect some extra keys:
 *       - export: The machine name of the export type
 *       - export_key: The name of the variable holding the export
 *       - name: The machine name of the export.
 */
function hook_fabricator_blueprints() {

  $blueprints = array();

  $blueprints['example_content_type'] = array(
    'name' => t('Example content type'),
    'type' => 'content_type',
    'file' => 'blueprints/example_content_type.php',
    'parser' => CONFIG_PARSER_PHP,
  );

  $blueprints['example_ctools'] = array(
    'name' => t('Example Chaos Tools'),
    'type' => 'ctools',
    'file' => 'blueprints/example_view.php',
    'parser' => CONFIG_PARSER_PHP,
    // Ctools exports also require the following export keys.
    'ctools' => array(
      'export' => 'views_view',
      'export_key' => 'view',
      'name' => 'fabricator_example_view',
    ),
  );

  return $blueprints;
}

/**
 * Defines factories for building blueprints.
 *
 * Factories build specific blueprint "types", so their key must
 * correspond to the type used in hook_fabricator_blueprints().
 *
 * @return array
 *   An array whose keys are factory names and whose corresponding values
 *   are arrays containing the following key-value pairs:
 *   - name: The human-readable name of the component, to be shown on the
 *     permission administration page. This should be wrapped in the t()
 *     function so it can be translated.
 *   - class: The FQN of the class which handles the factory. This class must
 *     implement Drupal\fabricator\Factory\FactoryInterface.
 */
function hook_fabricator_factories() {

  $factories = array();

  $factories['content_type'] = array(
    'name' => t('Content Type'),
    'class' => '\Drupal\fabricator\Factory\ContentType',
  );

  return $factories;
}
