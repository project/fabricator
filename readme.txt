Fabricator
----------

Fabricator is a build tool for Drupal site building components. Fabricator
components, "Blueprints", can be provided by individual modules, and then
built either programatically or using the Fabricator UI module.

Blueprints can be a single installable "thing" (e.g. a Content type with
fields, or a Ctools exportable), or can also include Components, which are
merely includable (but not "buildable") blueprints. For example, a content type
Blueprint Recipe may include a base content type Component, as well as several
Field Components. These same Components can also be used elsewhere. Ctools
exports do not support inclusion of components.

Features
--------

Fabricator currently supports the following:

- Factories for Content Types, Variables and Chaos Tools exports from Blueprints
- File type parsers for PHP & YAML
- Extensible, hook-based Factory and Parser mechanism
- Install Ctools exports without automatic inclusion
- Fabricator vs Features
- Fabricator and Features have some cross-over, but are designed for different
  use cases. An example config management workflow could be to use Fabricator
  to rapidly build content types, and use Features for deployment only.

Known issues and caveats
------------------------

Fabricator works but is currently in development. It is missing some useful
 features, and has some caveats.

- Components don't check module dependencies. This is planned, but not currently
  implemented. The workaround is to place components in modules, then
  specify dependencies in that module's info file.
- Blueprints will prevent duplicate installation of the base type (e.g. a
  content type), but otherwise have no collision checking (though fields will
  be reused rather than fail).
- Blueprints cannot be deleted from the Fabricator UI. However, unlike Features
  or normal ctools exports, Fabricator does not tell Drupal that your components
  are "in code". They get treated like any other database entry and are deletable.
- YAML blueprints require the Symfony YAML parser to be available, however
  there is no explicit check that this is present. The easiest way to ensure
  this is present is to install and setup Composer Manager.
