<?php

/**
 * @file
 * Administration function for Fabricator.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

use Drupal\fabricator\Component\ComponentManager;
use Drupal\fabricator\Component\BlueprintOps;
use Drupal\fabricator\Component\BlueprintStatus;

/**
 * Page callback for the Fabricator overview page.
 *
 * @return string
 *   Renderable output.
 * @throws Exception
 */
function fabricator_ui_admin_blueprint_overview() {
  drupal_add_css(drupal_get_path('module', 'fabricator_ui') . '/fabricator_ui.css');

  $blueprints = ComponentManager::init()->getBlueprints();

  $rows = array();

  $header = array(
    'name' => t('Name'),
    'desc' => t('Description'),
    'type' => t('Type'),
    'provider' => t('Provider'),
    'status' => t('Status'),
    'operations' => t('Operations'),
  );

  if (!empty($blueprints)) {
    $sorted = array();

    foreach ($blueprints as $key => $blueprint) {
      if (!isset($blueprint['group'])) {
        $blueprint['group'] = t('General');
      }

      $sorted[$blueprint['group']][$key] = $blueprint;
    }

    foreach ($sorted as $group_key => $group) {

      $rows[] = array(
        'name' => array(
          'data' => check_plain($group_key),
          'colspan' => 6,
          'class' => array('group-name'),
        ),
      );

      foreach ($group as $key => $blueprint) {

        $status = '<em>' . t('invalid/missing') . '</em>';
        $ops = array();

        try {
          $status = BlueprintStatus::init()->getReadableStatus($key);
          $ops = BlueprintOps::init()->getBlueprintOperations($key);
        }
        catch (\Drupal\fabricator\Exception\FabricatorException $e) {
          drupal_set_message(format_string('Error loading !key: !message', array(
            '!key' => $key,
            '!message' => $e->getMessage(),
          )), 'error');
        }

        $provider_info = system_get_info('module', $blueprint['provider']);

        $rows[] = array(
          'name' => array(
            'data' => check_plain($blueprint['name']),
          ),
          'desc' => array(
            'data' => isset($blueprint['description']) ? check_plain($blueprint['description']) : '',
          ),
          'type' => array(
            'data' => check_plain($blueprint['type']),
          ),
          'provider' => array(
            'data' => check_plain($provider_info['name']),
          ),
          'status' => array(
            'data' => $status,
          ),
          'operations' => array(
            'data' => implode(' | ', $ops),
          ),
        );
      }

    }
  }

  $vars = array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('No blueprints found.'),
  );

  return theme('table', $vars);
}

/**
 * Page callback for the Fabricator overview page.
 *
 * @return string
 *   Renderable output.
 * @throws Exception
 */
function fabricator_ui_admin_factory_overview() {

  $factories = \Drupal\fabricator\Factory\FactoryManager::init()->getFactories();

  $rows = array();

  $header = array(
    'name' => t('Name'),
    'provider' => t('Provider'),
  );

  if (!empty($factories)) {
    foreach ($factories as $key => $factory) {

      $provider_info = system_get_info('module', $factory['provider']);

      $rows[] = array(
        'name' => array(
          'data' => check_plain($factory['name']),
        ),
        'provider' => array(
          'data' => check_plain($provider_info['name']),
        ),
      );
    }
  }

  $vars = array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('No factories found.'),
  );

  return theme('table', $vars);
}

/**
 * View a blueprint.
 *
 * @param string $blueprint_name
 *   Blueprint name.
 *
 * @return string
 *   Renderable output.
 */
function fabricator_ui_admin_blueprint($blueprint_name) {
  $blueprint_name = check_plain($blueprint_name);

  // Invalid names shouldn't do anything.
  if (!ComponentManager::init()->isValid($blueprint_name, FABRICATOR_TYPE_BLUEPRINT)) {
    drupal_not_found();
  }

  drupal_set_title($blueprint_name);

  drupal_add_css(drupal_get_path('module', 'fabricator_ui') . '/fabricator_ui.css');

  $settings = ComponentManager::init()->getComponentSettings($blueprint_name, FABRICATOR_TYPE_BLUEPRINT);

  $output = fabricator_ui_admin_blueprint_print_row($settings);

  return $output;
}

/**
 * Helper to print blueprint information.
 *
 * @param array $settings
 *   Settings to print.
 *
 * @return string
 *   Formatted as a string
 */
function fabricator_ui_admin_blueprint_print_row($settings) {
  $output = '<ul>';

  foreach ($settings as $key => $setting) {

    if (is_array($setting)) {
      $output .= fabricator_ui_admin_blueprint_print_row($setting);
    }

    else {
      $output .= '<li><strong>' . check_plain($key) . '</strong> ' . $setting . '</li>';
    }

  };

  $output .= '</ul>';

  return $output;
}

/**
 * Page callback for operations.
 *
 * @param string $blueprint_name
 *   Blueprint name.
 * @param string $op
 *   Operation to run
 */
function fabricator_ui_admin_blueprint_op($blueprint_name, $op) {

  $blueprint_name = check_plain($blueprint_name);

  // Invalid names shouldn't do anything.
  if (!ComponentManager::init()->isValid($blueprint_name, FABRICATOR_TYPE_BLUEPRINT)) {
    drupal_not_found();
  }

  \Drupal\fabricator\Fabricator::init()->make($blueprint_name, $op);

  drupal_goto('admin/structure/fabricator');
}

/**
 * Page callback for operations.
 *
 * @param string $blueprint_name
 *   Blueprint name.
 */
function fabricator_ui_admin_blueprint_install($form, $form_state, $blueprint_name) {

  $blueprint_name = check_plain($blueprint_name);

  // Invalid names shouldn't do anything.
  if (!ComponentManager::init()->isValid($blueprint_name, FABRICATOR_TYPE_BLUEPRINT)) {
    drupal_not_found();
  }

  $blueprint_settings = ComponentManager::init()->getComponentSettings($blueprint_name, FABRICATOR_TYPE_BLUEPRINT);

  $form = array();
  $form['blueprint'] = array(
    '#type' => 'value',
    '#value' => $blueprint_name,
  );

  $form['about'] = array(
    '#markup' => '<p><strong>' . t('Name') . '</strong>: ' . $blueprint_settings['name'] . '<br /><strong>' . t('Type') . '</strong>: ' . $blueprint_settings['type'] . '<br /><strong>' . t('Description') . '</strong>: ' . $blueprint_settings['description'] . '</p>',
  );

  return confirm_form(
    $form,
    t('You are about to install the following item:'),
    'admin/structure/fabricator',
    t('This action cannot be undone.'),
    t('Install'),
    t('Cancel')
  );
}

/**
 * Page callback for operations.
 */
function fabricator_ui_admin_blueprint_install_submit($form, $form_state) {

  $blueprint_name = check_plain($form_state['values']['blueprint']);

  // Invalid names shouldn't do anything.
  if (!ComponentManager::init()->isValid($blueprint_name, FABRICATOR_TYPE_BLUEPRINT)) {
    drupal_not_found();
  }

  \Drupal\fabricator\Fabricator::init()->make($blueprint_name, 'install');

  drupal_goto('admin/structure/fabricator');
}

/**
 * Overview page for components.
 *
 * @return string
 *   Renderable output.
 *
 * @throws Exception
 */
function fabricator_ui_admin_component_overview() {
  drupal_add_css(drupal_get_path('module', 'fabricator_ui') . '/fabricator_ui.css');

  $components = \Drupal\fabricator\Component\ComponentManager::init()->getComponents();

  $rows = array();

  $header = array(
    'name' => t('Name'),
    'machine_name' => t('Machine name'),
    'provider' => t('Provider'),
    'description' => t('Description'),
  );

  if (!empty($components)) {

    $sorted = array();

    foreach ($components as $key => $component) {
      if (!isset($component['group'])) {
        $component['group'] = t('General');
      }

      $sorted[$component['group']][$key] = $component;
    }

    foreach ($sorted as $group_key => $group) {

      $rows[] = array(
        'name' => array(
          'data' => check_plain($group_key),
          'colspan' => 4,
          'class' => array('group-name'),
        ),
      );

      foreach ($group as $key => $component) {

        $provider_info = system_get_info('module', $component['provider']);

        $rows[] = array(
          'name' => array(
            'data' => check_plain($component['name']),
          ),
          'machine_name' => array(
            'data' => check_plain($component['machine_name']),
          ),
          'provider' => array(
            'data' => check_plain($provider_info['name']),
          ),
          'description' => array(
            'data' => isset($component['description']) ? check_plain($component['description']) : '',
          ),
        );
      }
    }
  }

  $vars = array(
    'rows' => $rows,
    'header' => $header,
    'empty' => t('No components found.'),
  );

  return theme('table', $vars);
}
