<?php

/**
 * @file
 * Contains an example content type
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

$export = array(

  // Default settings.
  'type' => 'fabricator_example',
  'name' => t('Fabricator example type'),
  'base' => 'node_content',
  'description' => t('Example Fabricator content type'),
  'custom' => 1,
  'modified' => 0,
  'locked' => 0,
  'disabled' => 0,
  'is_new' => 1,
  'has_title' => 1,
  'title_label' => 'Title',

  // Add a body field?
  'body_field' => TRUE,

  // RDF Mappings.
  'rdf_mappings' => array(
    array(
      'type' => 'node',
      'bundle' => 'fabricator_example',
      'mapping' => array(
        'title' => array(
          'predicates' => array(
            0 => 'schema:name',
          ),
        ),
        'field_image' => array(
          'predicates' => array(
            0 => 'og:image',
            1 => 'rdfs:seeAlso',
          ),
          'type' => 'rel',
        ),
      ),
    ),
  ),

  // Variables.
  'variables' => array(
    'node_options' => array('status'),
    'comment' => COMMENT_NODE_HIDDEN,
    'node_submitted' => FALSE,
  ),

  // Fields.
  'fields' => array(
    'field_image' => array(
      'field_name' => 'field_image',
      'type' => 'image',
      'cardinality' => 1,
      'locked' => FALSE,
      'indexes' => array('fid' => array('fid')),
      'settings' => array(
        'uri_scheme' => 'public',
        'default_image' => FALSE,
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
      ),
    ),
  ),
);
