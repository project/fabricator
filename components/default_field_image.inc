<?php

/**
 * @file
 * Contains a default image field.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */

$export = array(
  // Fields.
  'fields' => array(
    'field_image' => array(
      'field_name' => 'field_image',
      'type' => 'image',
      'cardinality' => 0,
      'locked' => FALSE,
      'indexes' => array('fid' => array('fid')),
      'settings' => array(
        'uri_scheme' => 'public',
        'default_image' => FALSE,
      ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
      ),
    ),
  ),
);
