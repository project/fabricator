<?php

/**
 * @file
 * Contains a default content type definition.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 * @copyright Copyright(c) 2014 Christopher Skene and PreviousNext
 */


$export = array(

  // Default settings.
  'type' => NULL,
  'name' => NULL,
  'base' => 'node_content',
  'description' => NULL,
  'custom' => 1,
  'modified' => 0,
  'locked' => 0,
  'disabled' => 0,
  'is_new' => 1,
  'has_title' => 1,
  'title_label' => 'Title',

  // Add a body field?
  'body_field' => TRUE,

  // RDF Mappings.
  'rdf_mappings' => array(
    array(
      'type' => 'node',
      'bundle' => 'fabricator_example',
      'mapping' => array(
        'title' => array(
          'predicates' => array(
            0 => 'schema:name',
          ),
        ),
      ),
    ),
  ),

  // Variables.
  'variables' => array(
    'node_options' => array('status'),
    'comment' => COMMENT_NODE_HIDDEN,
    'node_submitted' => FALSE,
  ),
);
